#include<stdio.h>
int main()
{
    int a[10][10],i,j,r,c,t[10][10];
    printf("Enter the number of rows in the array \n");
    scanf("%d",&r);
    printf("Enter the number of columns in the array \n");
    scanf("%d",&c);
    printf("Enter the elements \n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    printf("The matrix entered:\n");
	for(i=0;i<r;i++)
	{
		for(j=0;j<c;j++)
		{
			printf("%d ",a[i][j]);
		}
		printf("\n");
	}
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            t[j][i]=a[i][j];
            
        }
    }
    printf("Transpose of the matrix\n");
	for(i=0;i<r;i++)
	{
		for(j=0;j<c;j++)
		{
			printf("%d ",t[i][j]);
		}
		printf("\n");
	}
	return 0;
}