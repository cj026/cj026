#include<stdio.h>
void swap (int*x,int*y)
{
    int temp ;
    temp = *x;
    *x=*y ;
    *y=temp ;
}
int main ()
{
    int num1,num2 ;
    printf("enter the value of first number");
    scanf("%d",&num1);
    printf("enter the value of second number");
    scanf("%d",&num2);
    printf(" before swaping num1 is : %d, and num 2 is : %d\n",num1,num2);
    swap(&num1,&num2);
    printf(" after swaping num1 is : %d, and num 2 is : %d\n",num1,num2);
    return 0 ;
}
